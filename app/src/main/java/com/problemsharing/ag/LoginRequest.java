package com.problemsharing.ag;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saria on 11/11/16.
 */
public class LoginRequest extends StringRequest {
    private Map<String, String> params;

    public LoginRequest(String email,  Response.Listener<String> listener) {
        super(Request.Method.POST, Config.LOGIN_URL, listener, null);
        params=new HashMap<>();
        params.put("email", email);

    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}

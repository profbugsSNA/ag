package com.problemsharing.ag;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.problemsharing.ag.models.Problem;
import com.problemsharing.ag.models.User;
import com.problemsharing.ag.utilities.SessionManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saria on 11/12/16.
 */
public class ProblemRequest extends StringRequest {
    private Map<String, String> params;

    public ProblemRequest(Problem problem, Response.Listener<String> listener) {
        super(Request.Method.POST, Config.Problem_ControlURL, listener, null);
        params = new HashMap<>();
        params.put("title", problem.getTitle());
        params.put("problem", problem.getStatement());
        params.put("place", problem.getPlace());

        if (problem.getUser()!=null&&problem.getUser().getId() > 0) {
            params.put("user", Integer.toString(problem.getUser().getId()));
        }
        params.put("category", Integer.toString(problem.getCategory().getId()));
        if(problem.getImage()!=null){
            if(problem.getImage().getEncodedImage()!=null&&problem.getImage().getName()!=null){
                params.put("encoded_string", problem.getImage().getEncodedImage());
                params.put("image_name", problem.getImage().getName());
            }
        }
        System.out.println(params);
    }
    public ProblemRequest(Response.Listener<String> listener){
        super(Method.GET, Config.Problem_ControlURL, listener, null);
     }
    //here user problems specific to user id will be fetched
    public ProblemRequest(Response.Listener<String> listener, String url, String Id){
        super(Method.GET, url+Id, listener, null);
    }
    public ProblemRequest(Response.Listener<String> listener, String url, Problem problem){
        super(Request.Method.POST, url, listener, null);
        params = new HashMap<>();
        params.put("id",     Integer.toString(problem.getId()));
        params.put("title", problem.getTitle());
        params.put("problem", problem.getStatement());
        params.put("place", problem.getPlace());
        params.put("category", Integer.toString(problem.getCategory().getId()));
        System.out.println(params);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}

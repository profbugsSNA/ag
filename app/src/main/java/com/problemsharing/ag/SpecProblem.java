package com.problemsharing.ag;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.problemsharing.ag.models.Category;
import com.problemsharing.ag.models.Problem;
import com.problemsharing.ag.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpecProblem extends AppCompatActivity {
    private int MapRequest = 1;
    EditText problemTitle, problemStatement;
    Spinner problemCategory;
    TextView problemLocation;
    CheckBox solved;
    Button updateproblem, changeLocation;
    private List<Category> categories;
    private StringRequest request;
    private RequestQueue requestQueue;
    private String problemId;
    JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spec_problem);
         problemTitle = (EditText) findViewById(R.id.problemTitle);
        problemStatement = (EditText) findViewById(R.id.problemDetails);
        problemCategory = (Spinner) findViewById(R.id.problemCateg);
        problemLocation = (TextView) findViewById(R.id.problemLocation);
        solved = (CheckBox) findViewById(R.id.problemStatus);
        problemId = getIntent().getExtras().get("id").toString();

        getProblem();
        updateproblem = (Button) findViewById(R.id.updateProblem);
        changeLocation = (Button) findViewById(R.id.changeLocation);

        changeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                Intent intent;
                try {

                    intent = intentBuilder.build(v.getContext());
                    startActivityForResult(intent, MapRequest);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        updateproblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!problemId.isEmpty() && !problemTitle.getText().toString().isEmpty() && !problemStatement.getText().toString().isEmpty() && !problemLocation.getText().toString().isEmpty()) {
                    Category category = (Category) problemCategory.getSelectedItem();
                    Problem problem =new Problem();
                    problem.setId(Integer.parseInt(problemId));
                    problem.setTitle(problemTitle.getText().toString());
                    problem.setCategory(category);
                    problem.setPlace(problemLocation.getText().toString());
                    problem.setStatement(problemStatement.getText().toString());
                    updateProblem(problem);
                } else {

                    Toast.makeText(getApplicationContext(), Messages.fillBlanks, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void getProblem() {
        Response.Listener<String> responseListener = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    System.out.println("Json object " + jsonObject);
                    JSONArray arr = jsonObject.getJSONArray(Config.problemResult);
                    obj = arr.getJSONObject(0);
                    problemTitle.setText(getTitle(obj));
                    problemStatement.setText(getStatement(obj));
                    problemLocation.setText(getPlace(obj));
                    if (getStatus(obj)) {
                        solved.setChecked(true);
                    }
                    String categoryName = getCategory(obj);
                    Category categ = new Category();

                    categ.setName(categoryName);
                    categ.setId(Integer.parseInt(obj.getString("categoryId")));
                    getCategoryData(categ);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        };

        ProblemRequest request = new ProblemRequest(responseListener, Config.Specific_PROBLEM_URL, problemId);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request);
    }

    protected void getCategoryData(final Category category) {
        categories = new ArrayList<>();
        request = new StringRequest(Config.Category_ControlURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response);
                            JSONArray arr = json.getJSONArray(Config.categoryResult);

                            populateCategories(arr, category);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    protected void populateCategories(JSONArray arr, Category category) {
        try {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                Category categoryq = new Category(obj.getInt(Config.categoryId), obj.getString(Config.categoryName));
                categories.add(categoryq);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_dropdown_item, categories);
        problemCategory.setAdapter(adapter);
        for (Category categ : categories) {
            if (categ.getId() == category.getId()) {
                problemCategory.setSelection(adapter.getPosition(categ));

            }
        }


    }

    private String getTitle(JSONObject j) {
        String title = null;
        try {
            title = j.getString(Config.problemTitle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title;
    }

    private String getStatement(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString(Config.problemStatment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private String getCategory(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString(Config.problemCategory);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private String getPlace(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString(Config.problemPlace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private boolean getStatus(JSONObject j) {
        boolean statement = false;
        try {
            if (j.getString(Config.problemStatus).equals("1")) statement = true;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private void updateProblem(Problem problem) {
        Response.Listener<String> response = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    System.out.println("Result " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("Result " + jsonObject);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        Toast.makeText(getApplicationContext(), Messages.updateProblemSuccess, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), Messages.updateProblemFailed, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
             // System.out.println(problem.toString());
            ProblemRequest request = new ProblemRequest(response,Config.Update_Problem_URL,problem);
            requestQueue.add(request);
        }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MapRequest) {
            System.out.println("Result OK");
            if (data != null && resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getApplicationContext());
                System.out.println("Map data " + place.getAddress());
                problemLocation.setText(place.getAddress());

            }
        }
    }
}



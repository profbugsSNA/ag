package com.problemsharing.ag;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.problemsharing.ag.fragments.AddProblemFragment;
import com.problemsharing.ag.fragments.HomeFragment;
import com.problemsharing.ag.fragments.ProfileFragment;
import com.problemsharing.ag.utilities.SessionManager;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

public class ProblemsActivity extends AppCompatActivity {
    BottomBar bottomBar;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problems);
        sessionManager = new SessionManager(getApplicationContext());

        bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.menu_problems, new OnMenuTabClickListener() {

            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.Bottombaritemone) {
                    HomeFragment fragment = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
                } else if (menuItemId == R.id.Bottombaritemtwo) {
                    if (sessionManager.checkLogin()) {
                        finish();
                    } else {
                        ProfileFragment fragment = new ProfileFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
                    }

                } else if (menuItemId == R.id.Bottombaritemthree) {
                    if (sessionManager.checkLogin()) {
                        finish();
                    } else {
                        AddProblemFragment fragment = new AddProblemFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
                    }
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {

            }
        });

    }


}

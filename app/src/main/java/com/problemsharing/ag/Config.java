package com.problemsharing.ag;

/**
 * Created by saria on 11/7/16.
 */
public class Config {
    public static String ip="http://192.168.1.103:80/webservice/";
    public final static String Problem_ControlURL = ip+"problem_control.php";
    public final static String Update_Problem_URL = ip+"specificProblem.php";
    public final static String problemTitle="title";
    public final static String problemStatment="statement";
    public final static String problemCategory="category";
    public final static String problemPlace="place";
    public final static String problemStatus="solved";
    public final static String problemCreatedDate="createdAt";
    public final static String problemImagePath="path";
    public final static String problemResult="result";

    public final static String Category_ControlURL = ip+"category_control.php";
    public final static String categoryResult="result";
    public final static String categoryId="id";
    public final static String categoryName="name";
    public final static String REGISTER_URL=ip+"register.php";
    public final static String LOGIN_URL=ip+"login.php";

    public final static String USER_PROBLEM_URL = ip+"user_problem.php?user_id=";
    public final static String Specific_PROBLEM_URL = ip+"specificProblem.php?problem_id=";


}

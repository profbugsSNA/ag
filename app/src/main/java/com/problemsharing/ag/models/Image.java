package com.problemsharing.ag.models;

import android.graphics.Bitmap;

/**
 * Created by saria on 11/25/16.
 */
public class Image {
    private String name;
    private String encodedImage;
    private String path;
    private Bitmap image;
    public Image(String name, String encodedImage) {
        this.name = name;
        this.encodedImage = encodedImage;
    }

    public Image() {
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}

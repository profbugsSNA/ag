package com.problemsharing.ag.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.problemsharing.ag.MainActivity;
import com.problemsharing.ag.ProblemsActivity;
import com.problemsharing.ag.R;
import com.problemsharing.ag.SpecProblem;
import com.problemsharing.ag.models.Problem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 17.11.2016.
 */

public class ProfileCardAdapter extends RecyclerView.Adapter<ProfileCardAdapter.ViewHolder>{
    List<Problem> problems;
    ImageButton editButton, deleteButton, tick;
   // private final PublishSubject<String> onClickSubject = PublishSubject.create();

    public ProfileCardAdapter(String[] ids,String[] titles, String[] statements) {
        super();
        problems = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            Problem problem = new Problem();
            problem.setId(Integer.parseInt(ids[i]));
            problem.setTitle(titles[i]);
            problem.setStatement(statements[i]);
            problems.add(problem);
        }
    }
    /*
    AddProblemFragment fragment = new AddProblemFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
    * */
    View v;
    @Override
    public ProfileCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pr_card_layout, parent, false);
        ProfileCardAdapter.ViewHolder viewHolder = new ProfileCardAdapter.ViewHolder(v);
        editButton = (ImageButton) v.findViewById(R.id.editButton);
       // deleteButton = (ImageButton) v.findViewById(R.id.deleteButton);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return problems.size();
    }

    @Override
    public void onBindViewHolder(final ProfileCardAdapter.ViewHolder holder, final int position) {
        final Problem list = problems.get(position);

        holder.problemTitle.setText(list.getTitle());
        holder.problemStatement.setText(list.getStatement());
        holder.problemId.setText(Integer.toString(list.getId()));
        holder.problemTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("titel was pressed "+position);
            }
        });
        holder.problemStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("stat was pressed "+position);
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(v.getContext(), SpecProblem.class);

                intent.putExtra("position",position);
                intent.putExtra("id",list.getId());
                v.getContext().startActivity(intent);
            }
        });
       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         //       onClickSubject.onNext(element);
            }
        });*/
    }

    public void editProblem(View v){

    }

    public void delete(int position){
        problems.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView problemTitle;
        public TextView problemStatement;
        public TextView problemId;
        public ViewHolder(View itemView) {
            super(itemView);

            problemTitle = (TextView) itemView.findViewById(R.id.problemTitle);
            problemStatement = (TextView) itemView.findViewById(R.id.problemDetails);
            problemId=(TextView) itemView.findViewById(R.id.probId);
            System.out.println("Problem id "+problemId.getText());
            deleteButton = (ImageButton) itemView.findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(this);
           /* editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete(getPosition());
                }
            });*/
        }

        @Override
        public void onClick(View view) {
            delete(getPosition());
        }
    }
}

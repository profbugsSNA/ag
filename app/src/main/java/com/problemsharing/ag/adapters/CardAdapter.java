package com.problemsharing.ag.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.problemsharing.ag.R;
import com.problemsharing.ag.models.Image;
import com.problemsharing.ag.models.Problem;
import com.problemsharing.ag.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by saria on 11/13/16.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    List<Problem> problems;
    private ImageLoader imageLoader;
    private LayoutInflater inflater;
    private Context context;
    private DisplayImageOptions options;
    ImageButton  tick;
    public CardAdapter(String[] titles, String[] statements, String[] imagePaths, String[] places,String[] solved, Context context) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        problems = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            Problem problem = new Problem();
            Image image = new Image();
            if(imagePaths[i]!=null) {
                image.setPath(imagePaths[i]);
            }else{
                image.setPath("https://storage.googleapis.com/prestashop/img/no_image.png");
            }
            problem.setTitle(titles[i]);
            problem.setStatement(statements[i]);
            problem.setPlace(places[i]);
            problem.setImage(image);
            if(solved[i].equals("0")) problem.setStatus(false);
            else  problem.setStatus(true);

            problems.add(problem);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =inflater.inflate(R.layout.card_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Problem list = problems.get(position);
        if(list.isStatus()){
            tick.setVisibility(View.VISIBLE);
        }
        holder.problemTitle.setText(list.getTitle());
        holder.problemStatement.setText(list.getStatement());
        holder.problemPlace.setText(list.getPlace());
        //universal-image-loader package

        ImageSize targetSize = new ImageSize(100, 100); // result Bitmap will be fit to this size
        imageLoader.loadImage(list.getImage().getPath(), targetSize,options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                System.out.println("onloadcomplete "+imageUri);
                holder.problemImage.setImageBitmap(loadedImage);
            }

        });

    }

    @Override
    public int getItemCount() {
        return problems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView problemTitle;
        public TextView problemStatement;
        private ImageView problemImage;
        public TextView problemPlace;
        private ProgressBar progressBar;
        public ViewHolder(View itemView) {
            super(itemView);
            tick=(ImageButton) itemView.findViewById(R.id.tickButton);

            problemTitle = (TextView) itemView.findViewById(R.id.problemTitle);
            problemStatement = (TextView) itemView.findViewById(R.id.problemDetails);
            problemImage = (ImageView) itemView.findViewById(R.id.problemImage);
            problemPlace =(TextView) itemView.findViewById(R.id.problemPlace);
            progressBar= (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}

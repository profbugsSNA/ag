package com.problemsharing.ag;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.problemsharing.ag.fragments.HomeFragment;
import com.problemsharing.ag.utilities.SessionManager;
import com.problemsharing.ag.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {
    @NotEmpty(message = Messages.requiredField)
    private EditText email;
    @NotEmpty(message = Messages.requiredField)
    private EditText password;

    private Button login;

    private TextView registerLink;
    Validator validator;
    SessionManager sessionManager;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String userId = "userIdKey";;
    static SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager=new SessionManager(getApplicationContext());

        validator = new Validator(this);
        validator.setValidationListener(this);

        email = (EditText) findViewById(R.id.signinEmail);
        password = (EditText) findViewById(R.id.signinPassword);
        login = (Button) findViewById(R.id.signin);
        registerLink = (TextView) findViewById(R.id.registerLink);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        registerLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(intent);

            }
        });
        /*validate empty fields before sending request*/
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                validator.validate();

            }
        });

    }
     @Override
    public void onValidationSucceeded() {
       final String loginEmail = email.getText().toString();
        final String loginPassword = password.getText().toString();
        Response.Listener<String> responseListener = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        String password = jsonObject.getString("password");  /*if email exists get password*/
                        String id=jsonObject.getString("id");
                        String firstName=jsonObject.getString("firstName");
                        String lastName=jsonObject.getString("lastName");

                        if (Utility.checkHash(loginPassword, password)) {  /*check password hashes*/
                            sessionManager.createUserLoginSession(loginEmail, firstName, lastName, id);
                            Intent intent = new Intent(LoginActivity.this, ProblemsActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage(Messages.incorrectPassword)
                                    .setNegativeButton(Messages.retry, null)
                                    .create()
                                    .show();
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this); /*if email does not exists set alertDialog*/
                        builder.setMessage(Messages.emailFailed)
                                .setNegativeButton(Messages.retry, null)
                                .create()
                                .show();
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        };
        LoginRequest loginRequest = new LoginRequest(loginEmail, responseListener);
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        queue.add(loginRequest);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(LoginActivity.this, ProblemsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }
}


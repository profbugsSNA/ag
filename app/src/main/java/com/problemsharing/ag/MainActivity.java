package com.problemsharing.ag;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.problemsharing.ag.models.Category;
import com.problemsharing.ag.models.Problem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements Validator.ValidationListener {
    @NotEmpty(message = Messages.requiredField)
    private EditText title;
    @NotEmpty(message = Messages.requiredField)
    private EditText statement;
    private Button sendProblem;
    private StringRequest request;
    private RequestQueue requestQueue;
    private Spinner spinner;
    private List<Category> categories;

    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getCategoryData();
        validator = new Validator(this);
        validator.setValidationListener(this);

        title = (EditText) findViewById(R.id.problemTitle);
        statement = (EditText) findViewById(R.id.problemDetails);
        sendProblem = (Button) findViewById(R.id.sendProblem);
        spinner = (Spinner) findViewById(R.id.spinner);
        requestQueue = Volley.newRequestQueue(this);

        sendProblem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                validator.validate();

            }


        });

    }

    protected void getCategoryData() {
        categories = new ArrayList<>();
        request = new StringRequest(Config.Category_ControlURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response);
                            JSONArray arr = json.getJSONArray(Config.categoryResult);

                            populateCategories(arr);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    protected void populateCategories(JSONArray arr) {
        try {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                Category category = new Category(obj.getInt(Config.categoryId), obj.getString(Config.categoryName));
                categories.add(category);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        spinner.setAdapter(new ArrayAdapter<Category>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, categories));
    }

    @Override
    public void onValidationSucceeded() {
        Response.Listener<String> response = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        Toast.makeText(getApplicationContext(), "SUCCESS " + Messages.addProblemSuccess, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), ProblemsActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), "Failed " + Messages.addProblemFailed, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        Category category = (Category) spinner.getSelectedItem();
        Problem problem = new Problem();
        problem.setTitle(title.getText().toString());
        problem.setStatement(statement.getText().toString());
        problem.setCategory(category);
        //Source used: http://kosalgeek.com/connect-android-php-mysql-generic-asynctask/

        ProblemRequest request = new ProblemRequest(problem, response);
        requestQueue.add(request);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

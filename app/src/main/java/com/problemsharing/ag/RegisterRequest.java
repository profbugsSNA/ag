package com.problemsharing.ag;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saria on 11/10/16.
 */
public class RegisterRequest extends StringRequest {
    private Map<String, String> params;

    public RegisterRequest(String firstName, String lastName, String email,String password,  Response.Listener<String> listener) {
        super(Method.POST, Config.REGISTER_URL, listener, null);
        params=new HashMap<>();
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("email", email);
        params.put("password", password);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}

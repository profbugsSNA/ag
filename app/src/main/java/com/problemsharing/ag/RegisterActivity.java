package com.problemsharing.ag;

import android.content.Intent;
import android.renderscript.Script;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.problemsharing.ag.utilities.SessionManager;
import com.problemsharing.ag.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {


    @NotEmpty(message = Messages.requiredField)
    @Pattern(regex = "^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}", message = Messages.invalidFName)
    private EditText firstName;

    @NotEmpty(message = Messages.requiredField)
    @Pattern(regex = "^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}", message = Messages.invalidLName)
    private EditText lastName;

    @NotEmpty(message = Messages.requiredField)
    @Email(message = Messages.invalidEmail)
    private EditText email;

    @NotEmpty(message = Messages.requiredField)
    @Password(min = 5, message = Messages.invalidPassword)
    private EditText password;

    @ConfirmPassword(message = Messages.confirmPassword)
    private EditText confirmPassword;


    Validator validator;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        validator = new Validator(this);  /*initialize new validator for validating annotated register fields*/
        validator.setValidationListener(this);


        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.registerEmail);
        password = (EditText) findViewById(R.id.registerPassword);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        Button register = (Button) findViewById(R.id.register);

        /*when register button is clicked invoke validator*/
        register.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    /*
    * when validation is successful send request to webservice for registering new user
    * */
    @Override
    public void onValidationSucceeded() {
        final String fName = firstName.getText().toString();
        final String lName = lastName.getText().toString();
        final String registerEmail = email.getText().toString();

        String registerPassword = password.getText().toString();
        String confirmRegisterPassword = confirmPassword.getText().toString();
        sessionManager=new SessionManager(getApplicationContext());

        if (registerPassword.equals(confirmRegisterPassword)) {
            registerPassword = Utility.hash(registerPassword); //hash password

            /*create new response listener. if registration is successful then start LoginActivity if not set alertDialog*/
            Response.Listener<String> responseListener = new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    System.out.println("response ");
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("Response !!!!!"+jsonObject);
                        boolean success = jsonObject.getBoolean("success");
                        String id=jsonObject.getString("id");
                        if (success) {
                            sessionManager.createUserLoginSession(registerEmail,fName,lName,id);
                            Intent intent = new Intent(RegisterActivity.this, ProblemsActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            String message = jsonObject.getString("message");
                            if (message.equals("Email already exists")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage(Messages.emailExists)
                                        .setNegativeButton(Messages.retry, null)
                                        .create()
                                        .show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage(Messages.registerFailed)
                                        .setNegativeButton(Messages.retry, null)
                                        .create()
                                        .show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
           /*create new register request and add to request queue*/
            RegisterRequest registerRequest = new RegisterRequest(fName, lName, registerEmail, registerPassword, responseListener);
            RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
            queue.add(registerRequest);
        }
    }

    /*if validation failed then set errror messageds to fields*/
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

}

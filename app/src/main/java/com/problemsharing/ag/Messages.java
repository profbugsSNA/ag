package com.problemsharing.ag;

/**
 * Created by saria on 11/12/16.
 */
public class Messages {
    /*Login Register*/
    public static final String incorrectPassword="Şifrə yanlışdır";
    public static final String invalidPassword="Şifrə minumum 5 simvoldan ibarət olmalıdır";
    public static final String requiredField="Bu xana məcburidir";
    public static final String invalidEmail="E-poçt ünvanı yanlışdır";
    public static final String confirmPassword="Şifrələr uyğun deyil";
    public static final String registerFailed="Qeydiyyat uğursuz oldu.";
    public static final String retry="Yenidən cəhd edin";
    public static final String invalidFName="Ad hərflərdən ibarət olmalıdır";
    public static final String invalidLName="Soyad hərflərdən ibarət olmalıdır";
    public static final String emailExists="Bu e-poçt artıq mövcuddur";
    public static final String emailFailed="Bu E-poçt sistemdə  mövcud deyil ";

    /*problem*/
    public static final String addProblemSuccess="Problem əlavə olundu. Adminin təsdiqindən sonra hamı tərəfində görünəcək";
    public static final String addProblemFailed="Problem əlavə olunmadı. Yenidən cəhd edin";
    public static final String updateProblemSuccess="Yeniləndi";
    public static final String updateProblemFailed="Yenilənmədi. Yenidən cəhd edin";

    public static final String pickPlace="Zəhmət olmasa, ərazini xəritədə seçin";
    public static final String fillBlanks="Zəhmət olmasa, boş xanaları doldurun";

}

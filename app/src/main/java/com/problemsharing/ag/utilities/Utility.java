package com.problemsharing.ag.utilities;

import com.lambdaworks.crypto.SCryptUtil;

/**
 * Created by saria on 11/12/16.
 */
public class Utility {
    public static String hash(String password){
        String securepassword= SCryptUtil.scrypt(password,16,16,16);
        return securepassword;
    }
    public static boolean checkHash(String plainPassword, String hashedPassword){
        boolean matched = SCryptUtil.check(plainPassword, hashedPassword);
        return matched;
    }
}

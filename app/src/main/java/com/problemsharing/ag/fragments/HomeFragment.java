package com.problemsharing.ag.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.problemsharing.ag.Config;
import com.problemsharing.ag.ProblemRequest;
import com.problemsharing.ag.R;
import com.problemsharing.ag.adapters.CardAdapter;
import com.problemsharing.ag.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by saria on 11/12/16.
 */
public class HomeFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBar;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.home, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        progressBar=(ProgressBar) v.findViewById(R.id.progressBar1);
        layoutManager = new LinearLayoutManager(v.getContext());

        recyclerView.setLayoutManager(layoutManager);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(v.getContext()).build();
        ImageLoader.getInstance().init(config);

        getProblems(v);
        return v;
    }

    public void getProblems(View v) {
        Response.Listener<String> responseListener = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    JSONArray arr = jsonObject.getJSONArray(Config.problemResult);
                    populateCards(arr);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        };
        ProblemRequest request = new ProblemRequest(responseListener);
        RequestQueue requestQueue = Volley.newRequestQueue(v.getContext());
        requestQueue.add(request);
    }

    protected void populateCards(JSONArray arr) {
        try {
            String[] title = new String[arr.length()];
            String[] statement = new String[arr.length()];
            String[] imagePath = new String[arr.length()];
            String[] place=  new String[arr.length()];
            String[] solved=new String[arr.length()];

            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                title[i] = getTitle(obj);
                statement[i] = getStatement(obj);
                place[i]=getPlace(obj);
                solved[i]=getSolved(obj);

                if(!getImagePath(obj).equals("null")) {
                   // System.out.println("not null "+title[i]);
                    imagePath[i] = Config.ip + getImagePath(obj);
                }else{
                   // System.out.println("null "+title[i]);
                    imagePath[i]=null;
                }
            }
            adapter = new CardAdapter(title, statement, imagePath, place,solved, v.getContext());
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getTitle(JSONObject j) {
        String title = null;
        try {
            title = j.getString(Config.problemTitle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title;
    }

    private String getStatement(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString(Config.problemStatment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }

    private String getImagePath(JSONObject j) {
        String path = null;
        try {
            path = j.getString(Config.problemImagePath);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return path;
    }

    private String getDate(JSONObject j) {
        String date = null;
        try {
            date = j.getString(Config.problemCreatedDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return date;
    }

    private String getPlace(JSONObject j){
        String place = null;
        try {
           place = j.getString(Config.problemPlace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return place;
    }
    private String getSolved(JSONObject j){
        String statement = null;
        try {
            statement = j.getString("solved");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }
}

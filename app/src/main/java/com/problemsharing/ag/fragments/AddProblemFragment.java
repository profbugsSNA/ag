package com.problemsharing.ag.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.problemsharing.ag.Config;
import com.problemsharing.ag.Messages;
import com.problemsharing.ag.ProblemRequest;
import com.problemsharing.ag.ProblemsActivity;
import com.problemsharing.ag.R;
import com.problemsharing.ag.models.Category;
import com.problemsharing.ag.models.Image;
import com.problemsharing.ag.models.Problem;
import com.problemsharing.ag.models.User;
import com.problemsharing.ag.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by saria on 11/12/16.
 */
public class AddProblemFragment extends Fragment implements Validator.ValidationListener {
    private int MapRequest = 1;
    @NotEmpty(message = Messages.requiredField)
    private EditText title;
    @NotEmpty(message = Messages.requiredField)
    private EditText statement;
    private Button sendProblem, placePicker;

    @NotEmpty(message = Messages.pickPlace)
    private TextView placeInfo;
    private StringRequest request;
    private RequestQueue requestQueue;
    private Spinner spinner;
    private List<Category> categories;

    private ImageView showImage;
    private Button uploadPic;
    private Bitmap bitmap;
    private String imageName;
    private int PICK_IMAGE_REQUEST = 2;

    Validator validator;
    View v;
    SessionManager sessionManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_main, container, false);
        getCategoryData();
        sessionManager = new SessionManager(v.getContext());

        validator = new Validator(this);
        validator.setValidationListener(this);

        title = (EditText) v.findViewById(R.id.problemTitle);
        statement = (EditText) v.findViewById(R.id.problemDetails);
        sendProblem = (Button) v.findViewById(R.id.sendProblem);
        placePicker = (Button) v.findViewById(R.id.pickPlace);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        requestQueue = Volley.newRequestQueue(v.getContext());
        placeInfo = (TextView) v.findViewById(R.id.placeInfo);

        uploadPic = (Button) v.findViewById(R.id.uploadPic);
        showImage = (ImageView) v.findViewById(R.id.showImage);

        //listen to choose picture event
        //credit to https://www.simplifiedcoding.net/android-volley-tutorial-to-upload-image-to-server/
        uploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

            }
        });

        sendProblem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                validator.validate();

            }


        });
        placePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                Intent intent;
                try {

                    intent = intentBuilder.build(v.getContext());
                    startActivityForResult(intent, MapRequest);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;

    }

    protected void getCategoryData() {
        categories = new ArrayList<>();
        request = new StringRequest(Config.Category_ControlURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response);
                            JSONArray arr = json.getJSONArray(Config.categoryResult);

                            populateCategories(arr);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
    }

    protected void populateCategories(JSONArray arr) {
        try {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                Category category = new Category(obj.getInt(Config.categoryId), obj.getString(Config.categoryName));
                categories.add(category);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        spinner.setAdapter(new ArrayAdapter<Category>(v.getContext(), android.R.layout.simple_spinner_dropdown_item, categories));
    }

    @Override
    public void onValidationSucceeded() {
        final ProgressDialog loading = ProgressDialog.show(v.getContext(), "Yüklənir...", "Gözləyin...", false, false);
        Response.Listener<String> response = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                loading.dismiss();
                try {
                    System.out.println("Result " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("Result " + jsonObject);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        Toast.makeText(getContext(), Messages.addProblemSuccess, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(v.getContext(), ProblemsActivity.class));
                    } else {
                        Toast.makeText(v.getContext(), Messages.addProblemFailed, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        if (sessionManager.isUserLoggedIn()) {
            User user = new User();
            user.setId(Integer.parseInt(userDetails.get(SessionManager.User_Id)));
            Category category = (Category) spinner.getSelectedItem();
            Problem problem = new Problem();
            problem.setTitle(title.getText().toString());
            problem.setStatement(statement.getText().toString());
            problem.setCategory(category);
            problem.setPlace(placeInfo.getText().toString());
            problem.setUser(user);

            imageName = getImageName(userDetails.get(SessionManager.User_Id), problem.getTitle());
            Image image = new Image(imageName, getEncodedImage(bitmap));
            problem.setImage(image);

            // System.out.println(problem.toString());
            ProblemRequest request = new ProblemRequest(problem, response);
            requestQueue.add(request);
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(v.getContext());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(v.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MapRequest) {
            System.out.println("Result OK");
            if (data != null && resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, v.getContext());
                System.out.println("Map data " + place.getAddress());
                placeInfo.setText(place.getAddress());

            }
        } else if (requestCode == PICK_IMAGE_REQUEST) {
            System.out.println("image choose ok ");
            if (resultCode == Activity.RESULT_OK && data != null) {
                Uri filePath = data.getData();

                try {
                    //Getting the Bitmap from Gallery
                    bitmap = MediaStore.Images.Media.getBitmap(v.getContext().getContentResolver(), filePath);
                   //Setting the Bitmap to ImageView
                    showImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getEncodedImage(Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] bytes = byteArrayOutputStream.toByteArray();
            encodedImage = Base64.encodeToString(bytes, Base64.DEFAULT);
        }
        return encodedImage;
    }

    public String getImageName(String id, String title) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        String name = id + title.replaceAll("\\s+", "") + dt.format(new Date()).replaceAll("\\s+", "");
        return name;
    }

}

package com.problemsharing.ag.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.problemsharing.ag.Config;
import com.problemsharing.ag.ProblemRequest;
import com.problemsharing.ag.R;
import com.problemsharing.ag.adapters.ProfileCardAdapter;
import com.problemsharing.ag.utilities.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by saria on 11/12/16.
 */
public class ProfileFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    SessionManager sessionManager;
    TextView logout;
    private ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.out.println("On create called");

        View v=inflater.inflate(R.layout.profile, container, false);
        sessionManager=new SessionManager(v.getContext());
        progressBar=(ProgressBar)v.findViewById(R.id.progressBar2);

        recyclerView = (RecyclerView) v.findViewById(R.id.prf_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(v.getContext());

        recyclerView.setLayoutManager(layoutManager);
        getUserProblems(v);

        logout=(TextView)v.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logoutUser();
            }
        });
        return v;
    }

    public void getUserProblems(View v) {
        Response.Listener<String> responseListener = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    JSONArray arr = jsonObject.getJSONArray(Config.problemResult);
                    populateCards(arr);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        };
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        String userId = userDetails.get(SessionManager.User_Id);
        System.out.println("User ID"+userId);

        ProblemRequest request = new ProblemRequest( responseListener,Config.USER_PROBLEM_URL,userId);
        RequestQueue requestQueue = Volley.newRequestQueue(v.getContext());
        requestQueue.add(request);
    }

    protected void populateCards(JSONArray arr) {
        try {
            String[] title=new String[arr.length()];
            String[] statement=new String[arr.length()];
            String[] ids=new String[arr.length()];
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                title[i] = getTitle(obj);
                statement[i] = getStatement(obj);
                ids[i]=getIds(obj);
            }
            adapter = new ProfileCardAdapter(ids,title, statement);
            recyclerView.setAdapter(adapter);
            //recyclerView.
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getTitle(JSONObject j) {
        String title = null;
        try {
            title = j.getString(Config.problemTitle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return title;
    }
    private String getStatement(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString(Config.problemStatment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }
    private String getIds(JSONObject j) {
        String statement = null;
        try {
            statement = j.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statement;
    }


}
